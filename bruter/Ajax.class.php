<?php

include "Bruter.class.php";
class Ajax {
	public $Result;
	public $Bruter;
	public function __construct()
	{
		$this->Bruter = new Bruter();
	}
	public function runProcess($id)
	{
		$this->Response($this->Bruter->Process($id));
	}
	public function getLog($id)
	{
		$this->Response($this->Bruter->getLog($id));
	}
	public function setProcess()
	{
	
			if(!empty($_POST['passwords']['field'])) {
				$passwords = $this->passwords();
			}
			if(!empty($_POST['cookies'][0]['name']) ) {
				$cookies = $this->cookies($_POST['cookies']);
			} else {
				$cookies = array();
			}
			if(!empty($_POST['fields'][0]['name']) ) {
				$fields = $this->fields($_POST['fields']);
			} else {
				$fields = array();
			}
			if(!empty($_POST['sleep']) ) {
				$this->Bruter->sleep = $_POST['sleep'];
			} else {
				$this->Bruter->sleep = 0;
			}
			
			$this->Bruter->setUrl($_POST['form_url']);
			$this->Bruter->setLogin($_POST['login']);
			$this->Bruter->setPasswodrs($passwords);
			$this->Bruter->setForm($fields);
			$this->Bruter->setCookies($cookies);
			$this->Bruter->setIndication($_POST['indication']);
			if(!empty($_POST['editprocess'])) {
				$this->Response($this->Bruter->setProcess($_POST['editprocess']));
			} else {
				$id = $this->Bruter->newProcess();
				$this->Bruter->setProcess($id);
				$this->Response($id);
			}
	}
	public function fields($arr)
	{	
		$return = array();
		foreach($arr as $v)
		{
			if(!empty($v['name']) ){
				$return[$v['name']] = $v['value'];
			}	
		}
		return $return;
	}
	public function cookies($arr)
	{	
		$return = array();
		foreach($arr as $v)
		{
			if(!empty($v['name']) ){
				$return[$v['name']] = $v['value'];
			}	
		}
		return $return;
	}
	public function passwords()
	{
		$passwords = array();
		$passwords['field'] = $_POST['passwords']['field'];
		if(!empty($_FILES["passwords"])) {
			$dir = realpath(__DIR__).'/';
			foreach ($_FILES["passwords"]["error"] as $key => $error) 
			{
				if ($error == UPLOAD_ERR_OK) {
					$tmp_name = $_FILES["passwords"]["tmp_name"][$key];
					//$name = $_FILES["passwords"]["name"][$key];
					$passwords['values'] = file($_FILES["passwords"]["tmp_name"][$key]);
					//move_uploaded_file($tmp_name, $dir."temp/$process$name");
				}
			}
		
		} elseif(!empty($_POST['passwords']['spisok'])) {
				$passwords['values'] = explode("\r\n", $_POST['passwords']['spisok']);
			
		}
		return $passwords;
	}
	public function Request($arr)
	{
		$this->Result = json_decode($arr);
	}
	public function Response($arr)
	{
		$this->Result = json_encode($arr);
	}
	
}