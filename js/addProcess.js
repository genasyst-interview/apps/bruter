function setForm(type)
{
    $('.type1,.type2,.type3,.type4,.type5,.type6').css('display','none');
    $('.type'+type).css('display','');
}
function setPasswordType(type)
{
    $('.pass-type').css('display','none');
    $('#pass-type-'+type).css('display','');
}
function setAuthType(sel)
{
    $('.form_auth').css('display','');
    $('.form_auth').css('display','');

    var val = $(sel).val();
    if(val=='base_auth') {
        $('.form_auth').css('display','none');
        $('.form_auth').css('display','none');
    }


}
function dobcookie()
{
    if($("tr").is('.cookie')) {
        var count = $('.cookie:last').attr('count');
        var newcount = parseInt(count)+1;
        $("#udalitcookie").remove();

    } else {
        var newcount =0;
    }
    $(".dobcookie").before('<tr class="cookie" count="'+newcount+'"><td>' +
        '<input type="text" name="cookies['+newcount+'][name]" id="cookie'+newcount+'name"></td><td><input type="text" name="cookies['+newcount+'][value]" id="cookie'+newcount+'value" style="width: 80%;"><a id="udalitcookie" onclick="udalitcookie();"><i class="icon16 delete"></i></a></td></tr>');

}
function udalitcookie()
{
    $('.cookie:last').remove();
    $('.cookie:last').find('td:eq(1)').append('<a id="udalitcookie" onclick="udalitcookie();"><i class="icon16 delete"></i></a>');
}
function udalitpole()
{
    $('.pole:last').remove();
    $('.pole:last').find('td:eq(1)').append('<a id="udalitpole" onclick="udalitpole();"><i class="icon16 delete"></i></a>');
}
function dobpole()
{
    if($("tr").is('.pole')) {
        var count = $('.pole:last').attr('count');
        var newcount = parseInt(count)+1;
        $("#udalitpole").remove();

    } else {
        var newcount =0;
    }
    $(".dobpole").before('<tr class="pole" count="'+newcount+'"><td><input type="text" name="fields['+newcount+'][name]" id="fields'+newcount+'name"></td><td><input type="text" name="fields['+newcount+'][value]" id="fields'+newcount+'value" style="width: 80%;"><a id="udalitpole" onclick="udalitpole();"><i class="icon16 delete"></i></a></td></tr>');

}
$(function(){
    setForm(1);
    setPasswordType('list');
    $('.div-type a').click(function(){
        var type = $(this).data('type');

        var div_menu = $(this).closest('.div-type');
        div_menu.find('div').removeClass('active');
        $(this).parent().addClass('active');
        $('#form-type').val(type);
        setForm(type);
    });
    $('.div-pass a').click(function(){
        var type = $(this).data('type');

        var div_menu = $(this).closest('.div-pass');
        div_menu.find('div').removeClass('active');
        $(this).parent().addClass('active');
        $('#form-password-type').val(type);
        setPasswordType(type);
    });
});