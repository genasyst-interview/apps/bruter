<?php

class bruterConfig extends waAppConfig
{

    public function onCount()
    {
		
    }
	public function getCronJob($name = null)
	{
		static $tasks;
		if (!isset($tasks)) {
			$tasks = array();
			$path = $this->getAppConfigPath('cron');
			if (file_exists($path)) {
				$tasks = include($path);
			} else {
			$tasks = array();
			}
		}
		return $name?(isset($tasks[$name])?$tasks[$name]:null):$tasks;
	}
    
}