<?php

/**
 * Description of the Mazahaka 2 application
 * Описание приложения Гостевая книга 2
 * @see http://www.webasyst.com/framework/docs/dev/config/
 */
return array(
    // app name
    // название приложения
    'name'     => 'Bruter',
    // relative path to app icon file
    // относительный путь до иконки приложения
    'img'      => 'img/bruter.png',
    'icon'     => array(
        24 => 'img/bruter4.png',
        48 => 'img/bruter.png',
    ),
    // availability of extended access rights setup (as defined in config file bruterRightConfig.class.php)
    // есть детальная настройка прав приложения (описана в конфиге bruterRightConfig.class.php)
    'rights'   => true,
    // frontend availability
    // есть фронтенд
    'frontend' => true,
    // availability of design themes
    // темы дизайна
    'themes'   => true,
    // support for user authorization/registration in the frontend
    // поддерживает авторизацию/регистрацию пользователей во фронтенде
    'auth'     => true,
    // app version
    // версия приложения
    'version'  => '1.22',
    // last critical update
    // последнее важное обновление
    'critical' => '1.1',
    // developer name
    // разработчик
    'vendor'   => 'mazahaka',
);