<?php

/**
 * Default application settings
 * You can override them by creating file
 * wa-config/apps/bruter/config.php and specifying the desired values
 *
 * Настройки приложения по умолчанию
 * В конкретной установке их можно переопределить, создав файл
 * wa-config/apps/bruter/config.php и указав там нужные значения
 */
return array(
    // number of records per page in frontend
    // количество записей на одной странице во фронтенде
    'records_per_page' => 20,
	'records_domains' => 50
);