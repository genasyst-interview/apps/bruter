<?php

/**
 * Class bruterStepFormAuth
 */
class bruterStepFormAuth extends bruterStep
{

    /**
     * @var null
     */
    protected $Curl = null;
    /**
     * @var string
     */
    protected $form_url = '';
    /**
     * @var string
     */
    protected $host = '';
    /**
     * @var array
     */
    protected $cookies = array();
    /**
     * @var array
     */
    protected $form = array();
    /**
     * @var array
     */
    protected $login = array(
        'field' => 'val',
    );
    /**
     * @var array
     */
    protected $password = array(
        'field' => 'val',
    );
    /**
     * @var string
     */
    protected $indication_type = '';
    /**
     * @var string
     */
    protected $indication_value = '';
    /**
     * @var int
     */
    protected $maxredirs = 2;

    /**
     * bruterStepFormAuth constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setCurl();
        $this->setCookies($this->data['cookies']);
        $this->setUrl($this->data['protocol'], $this->data['domain'], $this->data['form_url']);
        $this->setForm($this->data['form']);
        $this->setLogin($this->data['login']);
        $this->setPassword($this->data['password']);
        $this->setIndication($data['indication']);


    }

    /**
     * @return array
     */
    public function getResult()
    {
        $this->firstRequest();
        $this->auth();
        $this->Indication();

        return array(
            'active' => $this->active,
            'status' => $this->status,
        );
    }

    /**
     * @param $protocol
     * @param $domain
     * @param $form_url
     */
    public function setUrl($protocol, $domain, $form_url)
    {
        $url = $protocol . $domain . $form_url;
        $this->form_url = $url;
        preg_match('@^(?:http://)?([^/]+)([\/].*)?@i', $domain, $host_arr);
        $this->host = $host_arr[1];
    }

    /**
     *
     */
    public function setCurl()
    {
        $this->Curl = new bruterCurl();
        $this->Curl->agent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36 OPR/27.0.1689.66 (Edition Yx)";
        $this->Curl->referer = "http://google.com/";
        $this->Curl->rawheaders["Pragma"] = "no-cache";
        $this->Curl->rawheaders["Accept"] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        $this->Curl->rawheaders["Accept-Encoding"] = 'gzip, deflate, lzma, sdch';
        $this->Curl->rawheaders["Accept-Language"] = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4';
        $this->Curl->rewheaders['Cache-Control'] = 'no-cache';
        $this->Curl->rewheaders['Connection'] = 'keep-alive';
    }

    /**
     * @param $arraylogin
     */
    public function setLogin($arraylogin)
    {
        $this->login = $arraylogin;
    }

    /**
     * @param $arraylogin
     */
    public function setPassword($arraylogin)
    {
        $this->password = $arraylogin;
    }

    /**
     * @param $cookies
     */
    public function setCookies($cookies)
    {
        $arr = array_merge($this->cookies, $cookies);
        $this->cookies = $arr;
    }

    /**
     * @param $fields
     */
    public function setForm($fields)
    {
        $arr = array_merge($this->form, $fields);
        $this->form = $arr;
    }

    /**
     * @param $indication
     */
    public function setIndication($indication)
    {
        $this->indication_type = $indication['type'];
        if ($this->indication_type == 'redirect') {
            $this->maxredirs = 0;
        }
        $this->indication_value = $indication['value'];
    }

    /**
     *
     */
    public function firstRequest()
    {
        $this->Curl->referer = $this->form_url;
        $this->Curl->rawheaders["Pragma"] = "no-cache";
        $this->Curl->cookies = $this->cookies;
        $this->Curl->fetchform($this->form_url);
        $this->Curl->setcookies();
        $this->setCookies($this->Curl->cookies);
        $FormParser = new bruterFormParser();
        if ($form = $FormParser->Parsing($this->Curl->resultsform)) {
            $this->setForm($form);
        }
        $this->html = $this->Curl->results;
        $this->headers = $this->Curl->headers;
        $this->response_code = $this->Curl->response_code;
    }

    /**
     *
     */
    public function auth()
    {


        $this->setForm($this->login);
        $this->setForm($this->password);
        $Curl = $this->Curl;
        $Curl->host = $this->host;
        //$Curl->cookies = $this->cookies;
        $Curl->referer = $this->form_url;
        $Curl->maxredirs = $this->maxredirs;
        var_dump($this->form);
        $Curl->submitform($this->form_url, $this->form);
        $Curl->setcookies();
        $this->setCookies($Curl->cookies);


        $this->html = $Curl->results;
        $this->headers = array_map('trim', $Curl->headers);
        $this->response_code = trim($Curl->response_code);

    }

    /**
     * @return mixed
     */
    public function Indication()
    {
        $type = $this->indication_type . 'Indication';

        return $this->$type();
    }

    /**
     * @return bool|int
     */
    public function redirectIndication()
    {
        $URI_PARTS = parse_url($this->form_url);
        $redirectaddr = '';
        foreach ($this->headers as $v) {
            if (preg_match('/^(?:http:\/\/)?([^\/]+)(\/.*\/)?/i', $this->form_url, $pat)) ;
            if (preg_match("/^(Location:|URI:)/i", $v)) {
                preg_match("/^(Location:|URI:)[ ]+(.*)/i", chop($v), $matches);
                if (!preg_match("|\:\/\/|", $matches[2])) {
                    $redirectaddr = $URI_PARTS["scheme"] . "://" . $this->host . "";
                    if (!preg_match("|^/|", $matches[2])) {
                        if (preg_match("|^\./|", $matches[2])) {
                            $redirectaddr =
                                $URI_PARTS["scheme"] . "://" . $pat[1] . $pat[2] . substr($matches[2], 2, strlen($matches[2]));
                        } else {
                            $redirectaddr = $URI_PARTS["scheme"] . "://" . $pat[1] . $pat[2] . $matches[2];
                        }

                    } elseif (preg_match("|^\./|", $matches[2])) {
                        $redirectaddr = $pat[0] . $pat[1] . $pat[2] . '!!!!' . substr($matches[2], 1, strlen($matches[2]));
                    } else {
                        $redirectaddr .= $matches[2];
                    }
                } else {
                    $redirectaddr = $matches[2];
                }
            }

        }
        var_dump($this->headers);
        echo $this->indication_value;
        if (preg_match('~' . $this->indication_value . '~i', $redirectaddr)) {
            $this->status = 1;

            return true;
        } else {
            return 0;
        }
    }

    /**
     *
     */
    public function textIndication()
    {

    }

    /**
     *
     */
    public function notextIndication()
    {

    }
}