<?php

/**
 * Class bruterStep
 */
class bruterStep
{
    /**
     * @var array
     */
    protected $data = array();
    /**
     * @var int
     */
    protected $status = 0;
    /**
     * @var int
     */
    protected $active = 1;

    /**
     * bruterStep constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public static function get($data)
    {
        $class = new static($data);

        return $class->getResult();
    }

    /**
     *
     */
    public function getResult()
    {
    }

}