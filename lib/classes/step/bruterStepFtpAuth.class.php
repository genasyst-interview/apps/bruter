<?php

/**
 * Class bruterStepFtpAuth
 */
class bruterStepFtpAuth extends bruterStep
{
    /**
     * @var null
     */
    protected $conn = null;

    /**
     * @return array
     */
    public function getResult()
    {
        $con = $this->connect($this->data['domain'], $this->data['port']);
        if ($con) {
            $this->auth();
        }

        return array(
            'active' => $this->active,
            'status' => $this->status,
        );

    }

    /**
     * @param $ftp_server
     * @param $port
     *
     * @return bool
     */
    public function connect($ftp_server, $port)
    {
        if (empty($port)) {
            $port = 21;
        }
        $conn_id = ftp_connect($ftp_server, $port);
        if ($conn_id) {
            $this->conn = $conn_id;

            return true;
        } else {
            $this->active = 0;

            return false;
        }
    }

    /**
     * @return bool
     */
    public function auth()
    {
        $auth = false;
        if (!empty($this->data['login']) && !empty($this->data['password'])) {
            $auth = @ftp_login($this->conn, $this->data['login'], $this->data['password']);
        }
        if ($auth) {
            $this->status = 1;

            return true;
        } else {
            return false;
        }
    }
}