<?php

/**
 * Class bruterRequestBody
 */
class bruterRequestBody
{

    // URL Data
    /**
     * @var string
     */
    protected $scheme = '';
    /**
     * @var string
     */
    protected $host = '';
    /**
     * @var string
     */
    protected $port = '';
    /**
     * @var string
     */
    protected $path = '';
    /**
     * @var string
     */
    protected $user = '';
    /**
     * @var string
     */
    protected $pass = '';
    /**
     * @var string
     */
    protected $query = '';
    /**
     * @var string
     */
    protected $fragment = '';


    /**
     * @var array
     */
    protected $headers = array();
    /**
     * @var array
     */
    protected $get_vars = array();
    /**
     * @var array
     */
    protected $post_vars = array();
    /**
     * @var array
     */
    protected $post_files = array();
    /**
     * @var array
     */
    protected $cookies = array();


    // Системные переменные
    /**
     * @var string
     */
    protected $_mime_boundary = '';
    /**
     * @var string
     */
    protected $_submit_type = '';
    /**
     * @var string
     */
    protected $_http_version = 'HTTP/1.1';


    /**
     * bruterRequestBody constructor.
     *
     * @param $host
     * @param $port
     */
    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->setDefaultHeaders();
        $this->setSubmitNormal();
    }

    /**
     *
     */
    public function setDefaultHeaders()
    {
        $this->headers = array();
        $this->headers['Host'] = $this->host;
        $this->headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        $this->headers['Accept-Language'] = 'ru,en;q=0.8';
        $this->headers['Cache-Control'] = 'max-age=0';
        $this->headers['Connection'] = 'close';
        $this->headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 YaBrowser/16.2.0.3539 Safari/537.36';

    }

    /**
     *
     */
    public function setSubmitNormal()
    {
        $this->_submit_type = "application/x-www-form-urlencoded";
    }

    /**
     *
     */
    public function setSubmitMultipart()
    {
        $this->_submit_type = "multipart/form-data";
        $this->_mime_boundary = "---WebKitFormBoundary" . md5(uniqid(microtime()));
    }

    /**
     * @param $host
     * @param $port
     *
     * @return string
     */
    public static function connect($host, $port)
    {
        return "CONNECT $host:$port HTTP/1.1\r\nConnection: Close\r\n\r\n\r\n";
    }

    /**
     * @param $name
     * @param $value
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * @param $data
     */
    public function setHeaders($data)
    {
        $this->headers = $data;
    }

    /**
     * @param $referer
     */
    public function setReferer($referer)
    {
        $this->headers['Referer'] = $referer;
    }

    /**
     * @param array $cookies
     */
    public function setCookies($cookies = array())
    {
        $this->cookies = $cookies;
    }

    /**
     * @return string
     */
    public function getCookies()
    {
        $cookie_header = '';
        foreach ($this->cookies as $k => $v) {
            $cookie_header .= $k . "=" . urlencode($v) . "; ";
        }
        $cookie_header .= substr($cookie_header, 0, -2);

        return $cookie_header;
    }

    /**
     * @param       $url
     * @param array $post_vars
     * @param array $post_files
     *
     * @return string
     */
    public function post($url, $post_vars = array(), $post_files = array())
    {
        settype($post_vars, 'array');
        settype($post_files, 'array');
        $post_body = $this->getPostBody($post_vars, $post_files);

        return $this->request($url, 'POST', $this->_submit_type, $post_body);
    }

    /**
     * @param        $url
     * @param string $content_type
     *
     * @return string
     */
    public function get($url, $content_type = '')
    {
        return $this->request($url, 'GET', $content_type, '');
    }

    /**
     * @param string $url
     * @param string $http_method
     * @param string $content_type
     * @param string $body
     *
     * @return string
     */
    public function request($url = '', $http_method = '', $content_type = '', $body = '')
    {
        $headers = $http_method . " " . $url . " " . $this->_http_version . "\r\n";
        // HOST
        if (!empty($URI_PARTS['host'])) {
            $header_host = $URI_PARTS['host'];
            if (!empty($URI_PARTS['port'])) {
                $header_host .= ":" . $URI_PARTS['port'];
            }
            $this->setHeader('Host', $header_host);
        }
        // COOKIE
        if (!empty($this->cookies)) {
            $this->setHeader('Cookies', $this->getCookies());
        }
        // CONTENT TYPE
        if (!empty($content_type)) {
            $content_header = $content_type;
            if ($content_type == "multipart/form-data") {
                $content_header .= "; boundary=" . $this->_mime_boundary;
            }
            $this->setHeader('Content-type', $content_header);
        }
        if (!empty($body)) {
            $this->setHeader('Content-length', strlen($body));
        }

        if (!empty($URI_PARTS['user']) || !empty($URI_PARTS['pass'])) {
            $this->setHeader('Authorization', 'Basic ' . base64_encode($URI_PARTS['user'] . ':' . $URI_PARTS['user']));
        }
        // Пишем заголовки
        foreach ($this->headers as $name => $val) {
            $headers .= $name . ': ' . $val . "\r\n";
        }
        $headers .= "\r\n";

        return $headers . $body;


    }

    /**
     * @param $post_vars
     * @param $post_files
     *
     * @return string
     */
    protected function getPostBody($post_vars, $post_files)
    {
        $post_string = '';
        if (count($post_vars) == 0 && count($post_files) == 0) {
            return '';
        }
        if (!empty($post_vars)) {
            $post_string .= $this->getPostVars($post_vars);
        }
        if ($this->_submit_type == "multipart/form-data" && !empty($post_files)) {
            $post_string .= $this->getPostFiles($post_files);
        }
        if ($this->_submit_type == "multipart/form-data") {
            $post_string .= "--" . $this->_mime_boundary . "--\r\n";
        }

        return $post_string;
    }

    /**
     * @param        $post_vars
     * @param string $start_key_name
     *
     * @return string
     */
    protected function getPostVars($post_vars, $start_key_name = '')
    {
        $post_string = '';
        switch ($this->_submit_type) {
            case "application/x-www-form-urlencoded":
                foreach ($post_vars as $key => $val) {
                    if (!empty($start_key_name)) {
                        $key = '[' . urlencode($key) . ']';
                    }
                    if (is_array($val)) {
                        $post_string .= $this->getPostVars($val, $start_key_name . $key);
                    } else {
                        $post_string .= $start_key_name . $key . "=" . urlencode($val) . "&";
                    }
                }
                break;

            case "multipart/form-data":
                foreach ($post_vars as $key => $val) {
                    if (!empty($start_key_name)) {
                        $key = '[' . $key . ']';
                    }
                    if (is_array($val)) {
                        $post_string .= $this->getPostVars($val, $start_key_name . $key);
                    } else {
                        $post_string .= "--" . $this->_mime_boundary . "\r\n";
                        $post_string .= "Content-Disposition: form-data; name=\"" . $start_key_name . $key . "\"\r\n\r\n";
                        $post_string .= "$val\r\n";
                    }
                }
                break;
        }

        return $post_string;
    }

    /**
     * @param        $post_files
     * @param string $start_key_name
     *
     * @return string
     */
    protected function getPostFiles($post_files, $start_key_name = '')
    {
        $post_string = '';
        foreach ($post_files as $key => $file_name) {
            if (!empty($start_key_name)) {
                $key = '[' . $key . ']';
            }
            if (is_array($file_name)) {
                $post_string .= $this->getPostFiles($file_name, $start_key_name . $key);
            } else {
                if (!is_readable($file_name)) {
                    continue;
                }
                $fp = fopen($file_name, "r");
                $file_content = fread($fp, filesize($file_name));
                fclose($fp);
                $base_name = basename($file_name);

                $post_string .= "--" . $this->_mime_boundary . "\r\n";
                $post_string .= "Content-Disposition: form-data; name=\"" . $start_key_name . $key . "\"; filename=\"$base_name\"\r\n\r\n";
                $post_string .= "$file_content\r\n";
            }
        }

        return $post_string;
    }

    /**
     * @param $url
     */
    protected function parseUrl($url)
    {
        $URI_PARTS = parse_url($url);
        foreach (array('scheme', 'host', 'port', 'user', 'pass', 'path', 'query', 'fragment') as $var) {
            if (!empty($URI_PARTS[$var])) {
                $this->$$var = $URI_PARTS[$var];
            }
        }
    }
}