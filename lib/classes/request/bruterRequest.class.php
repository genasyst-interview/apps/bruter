<?php

/**
 * Class bruterRequest
 */
class bruterRequest
{
    /**
     * @var null
     */
    protected $connect = null;
    /**
     * @var array
     */
    protected $cookies = array();
    /**
     * @var array
     */
    protected $headers = array();

    /**
     * bruterRequest constructor.
     *
     * @param $connect
     * @param $cookies
     * @param $headers
     */
    public function __construct($connect, $cookies, $headers)
    {
        $this->connect = $connect;
        $this->cookies = $cookies;
        $this->headers = $headers;
    }

    /**
     * @param $url
     * @param $post_data
     * @param $post_files
     *
     * @return mixed
     */
    public function post($url, $post_data, $post_files)
    {
        $request = new bruterRequestBody($url, 80);

        return $this->getConnect()->request($request->post($url, $post_data, $post_files));
    }

    /**
     * @param $url
     * @param $get_data
     *
     * @return mixed
     */
    public function get($url, $get_data)
    {
        $request = new bruterRequestBody($url, 80);

        return $this->getConnect()->request($request->get($url, $get_data));
    }

    /**
     * @param $method
     * @param $url
     * @param $data
     * @param $files
     *
     * @return mixed
     */
    public function request($method, $url, $data, $files)
    {
        if ($method == 'post') {
            return $this->post($url, $data, $files);
        } else {
            return $this->get($url, $data);
        }
    }

    /**
     * @return null
     */
    public function getConnect()
    {
        return $this->connect;
    }
}