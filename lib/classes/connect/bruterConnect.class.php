<?php

/**
 * Class bruterConnect
 */
class bruterConnect
{
    /**
     * @var null|resource
     */
    protected $connect = null;

    /**
     * @var string
     */
    protected $error_code = '';
    /**
     * @var string
     */
    protected $error_string = '';

    /**
     * bruterConnect constructor.
     *
     * @param $data
     */
    protected function __construct($data)
    {
        if (is_resource($data)) {
            $this->connect = $data;
        }
    }

    /**
     * @param $data
     *
     * @return bool|bruterConnect
     */
    public static function connect($data)
    {
        if (is_resource($data)) {
            return new self($data);
        } elseif (is_array($data)) {
            $conn = @fsockopen($data['host'], $data['port'], $error_code, $error_string, $data['connect_timeout']);
            if ($conn) {
                return new self($conn);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return bruterRequest
     */
    public function request()
    {
        return new bruterRequest($this);
    }

    /**
     * @return null|resource
     */
    public function getConnect()
    {
        return $this->connect;
    }
}