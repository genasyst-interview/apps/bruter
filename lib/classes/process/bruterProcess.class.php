<?php

/**
 * Class bruterProcess
 */
class bruterProcess
{

    /**
     * @var int
     */
    protected $id = 0;
    /**
     * @var int
     */
    protected $password_list_id = 0;
    /**
     * @var string
     */
    protected $type = '';
    /**
     * @var int
     */
    protected $active = 0;
    /**
     * @var int
     */
    protected $status = 0;
    /**
     * @var int
     */
    protected $step1 = 0;
    /**
     * @var int
     */
    protected $step2 = 0;
    /**
     * @var int
     */
    protected $step3 = 0;
    /**
     * @var int
     */
    protected $step4 = 0;
    /**
     * @var string
     */
    protected $indication_type = '';
    /**
     * @var array
     */
    protected $domains = array();
    /**
     * @var array
     */
    protected $passwords = array();
    /**
     * @var string
     */
    protected $protocol = '';

    /**
     * bruterProcess constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->password_list_id = $data['password_list_id'];
        $this->type = $data['type'];
        $this->active = $data['active'];
        $this->status = $data['status'];
        $this->step1 = $data['step1'];
        $this->step2 = $data['step2'];
        $this->step3 = $data['step3'];
        $this->step4 = $data['step4'];
        $this->indication_type = $data['indication_type'];
        $this->getDomains();
        $this->getPasswords();
    }

    /**
     *
     */
    public function getDomains()
    {
        $domainsModel = new bruterDomainModel();
        $this->domains = $domainsModel->getDomains($this->id, 0);
    }

    /**
     *
     */
    public function getPasswords()
    {
        $passModel = new bruterPasswordModel();
        $this->passwords = $passModel->getPasswordList($this->password_list_id);
    }
}