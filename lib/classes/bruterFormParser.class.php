<?

/**
 * Class bruterFormParser
 */
class bruterFormParser
{
    /**
     * @param $form
     *
     * @return mixed
     */
    public function form_all($form)
    {
        $types = $this->types;
        foreach ($types as $type_k => $type_v) {
            $tip = $this->type($form, $type_v);
            if (!empty($tip)) {
                $data[$type_k] = $this->type($form, $type_v);
            }
        }

        return $data;

    }

    /**
     * @param $form
     *
     * @return bool
     */
    public function Parsing($form)
    {
        preg_match_all('/<[\s]*input[^>].*[^>]*>/isU', $form, $arr);
        foreach ($arr['0'] as $input) {
            preg_match('/<[\s]*input[^>]+name[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $key);
            preg_match('/<[\s]*input[^>]+value[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $val);
            if (!empty($key[1])) {
                if (!empty($val[1])) {
                    $pole[$key[1]] = $val[1];
                } else {
                    $pole[$key[1]] = '';
                }
            }
        }
        if (!empty($pole)) {
            return $pole;
        } else {
            return false;
        }

    }

    /**
     * @param        $form
     * @param string $type
     *
     * @return mixed
     */
    public function type($form, $type = 'hidden')
    {
        if ($type == 'checkbox') {
            $r_type = $this->checkbox($form);
        } elseif ($type == 'select') {
            $r_type = $this->select($form);
        } elseif ($type == 'radio') {
            $r_type = $this->radio($form);
        } else {
            $regular = '/<[\s]*input[^>]*type[\s]*=[\s]*"[\s]*' . $type . '[\s]*"[^>]*>/isU';
            preg_match_all('' . $regular . '', $form, $type_arr);
            foreach ($type_arr['0'] as $input) {
                preg_match('/<[\s]*input[^>]+name[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $key);
                preg_match('/<[\s]*input[^>]+value[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $val);
                $r_type['' . $key['1'] . ''] = (string)$val['1'];
            }
        }

        return $r_type;
    }


    /**
     * @param $form
     *
     * @return mixed
     */
    public function checkbox($form)
    {
        preg_match_all('/<[\s]*input[^>]+type[\s]*=[\s]*"[\s].*[\s]*"[^>]*>/isU', $form, $checkbox_arr);
        foreach ($checkbox_arr['0'] as $input) {
            preg_match('/<[\s]*input[^>]+name[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $key);
            preg_match('/<[\s]*input[^>]+value[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $checkbox_val);
            $checkbox['' . $key[1] . ''][] = $checkbox_val[1];
            echo $checkbox_val[1] . '\n';
        }

        return $checkbox;
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public function select($form)
    {
        preg_match_all('/<[\s]*select[^>]+>(.+)<\/[\s]*select[\s]*>/isU', $form, $select_arr);
        foreach ($select_arr['0'] as $input) {
            preg_match('/<[\s]*select[^>]+name[\s]*=[\s]*"(.*)"[^>]*>/sU', $input, $name);
            preg_match_all('/<[\s]*option[^>]+value[\s]*=[\s]*"(.*)"[^>]*>/isU', $form, $option_arr);
            $select['' . $name[1] . ''] = $option_arr[1];
        }

        return $select;
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public function radio($form)
    {
        preg_match_all('/<[\s]*input[^>]*type[\s]*=[\s]*"[\s]*radio[\s]*"[^>]*>/isU', $form, $radio_arr);
        foreach ($radio_arr['0'] as $input) {
            preg_match('/<[\s]*input[^>]+name[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $key);
            preg_match('/<[\s]*input[^>]+value[\s]*=[\s]*"(.*)"[^>]*>/isU', $input, $val);
            $radio['' . $key['1'] . ''][] = (string)$val['1'];
        }

        return $radio;
    }


}