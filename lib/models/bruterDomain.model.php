﻿<?php

class bruterDomainModel extends waModel
{

    protected $table = 'bruter_domain';

    // Функция вырезает домен из произвольного адреса
    public function cutDomain($url)
    {
        preg_match('@^(?:http://)?([^/]+)([\/].*)?@i', mb_strtolower($url), $host_arr);
        $name = $host_arr[1];
        if (preg_match('/^www\./', $name)) {
            $name = preg_replace('/^www\./i', '', $name);
        }

        return $name;
    }

    public function getDomains($process_id = null, $status = 0)
    {
        return $this->getByField(
            array('process_id' => $process_id, 'status' => $status), true
        );
    }

    public function setDomains($list_id = null, $domains = array())
    {
        if (!empty($domains)) {
            $step = 1;
            foreach ($domains as $v) {
                $data = array();
                $data['domain'] = $this->cutDomain($v);
                $data['url'] = $v;
                $data['domain_list_id'] = $list_id;
                $data['step'] = $step++;
                $data['active'] = 1;
                $data['blocked'] = 0;
                $data['status'] = 0;
                $data['create_datetime'] = date("Y-m-d H:i:s");
                $this->insert($data);

            }
        }
    }
}