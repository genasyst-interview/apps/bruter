﻿<?php

class bruterPasswordModel extends waModel
{

    protected $table = 'bruter_password';

	public function getPasswordList($list_id=0)
	{
		if(!empty($list_id)) {
			$passwords = $this->getByField(
				array(
					'password_list_id' =>$list_id
				),true
			);
		} else {
			$passwords =array();
		}
		return $passwords ;

	}
}