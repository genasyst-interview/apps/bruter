﻿<?php

class bruterDomainListModel extends waModel
{
  
    protected $table = 'bruter_domain_list';

	public function getLists()
	{
		return $this->getAll(null,false);
	}
	public function addList($domains = array(), $name = '')
	{
		if(empty($name)) {
			$name = 'NewTempList-'.count($domains).'-'. date("Y-m-d_H:i:s");
		}
		$data = array(
			'name' => $name ,
			'create_datetime' => date("Y-m-d H:i:s")
		);
		$list_id = $this->insert($data);
		$domainModel = new bruterDomainModel();

		if(!empty($domains)) {
			$domainModel->setDomains($list_id,$domains);
		}

		$this->updateById($list_id,array('count_passwords' =>count($domains) ));
	}
     
   
}