﻿<?php

class bruterProcessModel extends waModel
{

    protected $table = 'bruter_process';

    public function getProcesses($status = null)
    {
        if (!empty($status)) {
            $processes = $this->getByField(
                array(
                    'status' => $status,
                ), true
            );
        } else {
            $processes = $this->getAll(null, false);

        }

        return $processes;

    }


}