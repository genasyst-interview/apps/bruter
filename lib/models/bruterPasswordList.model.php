﻿<?php

class bruterPasswordListModel extends waModel
{

    protected $table = 'bruter_password_list';

    public function getLists()
    {
        return $this->getAll(null, false);
    }

    public function addList($passwords = array(), $name = '')
    {
        if (empty($name)) {
            $name = 'NewTempList-' . count($passwords) . '-' . date("Y-m-d_H:i:s");
        }
        $data = array(
            'name'            => $name,
            'create_datetime' => date("Y-m-d H:i:s"),
        );
        $list_id = $this->insert($data);
        $passwordModel = new bruterPasswordModel();

        if (!empty($passwords)) {
            $step = 1;
            foreach ($passwords as $v) {
                $data = array(
                    'step'             => $step++,
                    'value'            => $v,
                    'md5'              => md5($v),
                    'create_datetime'  => date("Y-m-d H:i:s"),
                    'password_list_id' => $list_id,
                );
                $passwordModel->insert($data);
            }
        }

        $this->updateById($list_id, array('count_passwords' => count($passwords)));
    }


}