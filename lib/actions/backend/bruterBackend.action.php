<?php


class dbruterBackendAction extends waViewAction
{

	public function execute()
	{
		$status = waRequest::get('staus');
		$processModel = new bruterProcessModel();

		$processes = $processModel->getProcesses($status);
		$this->view->assign(array('processes'=>$processes));
	}
}