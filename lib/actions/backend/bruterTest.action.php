<?php

class httpRequest
{

    // URL Data
    protected $scheme = '';
    protected $host = '';
    protected $port = '';
    protected $path = '';
    protected $user = '';
    protected $pass = '';
    protected $query = '';
    protected $fragment = '';


    protected $headers = array();
    protected $get_vars = array();
    protected $post_vars = array();
    protected $post_files = array();
    protected $cookies = array();


    // Системные переменные
    protected $_mime_boundary = '';
    protected $_submit_type = '';
    protected $_http_version = 'HTTP/1.1';


    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->setDefaultHeaders();
        $this->setSubmitNormal();
    }

    public function setDefaultHeaders()
    {
        $this->headers = array();
        $this->headers['Host'] = $this->host;
        $this->headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        $this->headers['Accept-Language'] = 'ru,en;q=0.8';
        $this->headers['Cache-Control'] = 'max-age=0';
        $this->headers['Connection'] = 'close';
        $this->headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 YaBrowser/16.2.0.3539 Safari/537.36';

    }

    public function setSubmitNormal()
    {
        $this->_submit_type = "application/x-www-form-urlencoded";
    }

    public function setSubmitMultipart()
    {
        $this->_submit_type = "multipart/form-data";
        $this->_mime_boundary = "---WebKitFormBoundary" . md5(uniqid(microtime()));
    }

    public static function connect($host, $port)
    {
        return "CONNECT $host:$port HTTP/1.1\r\nConnection: Close\r\n\r\n\r\n";
    }

    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    public function setReferer($referer)
    {
        $this->headers['Referer'] = $referer;
    }

    public function setCookies($cookies = array())
    {
        $this->cookies = $cookies;
    }

    public function getCookies()
    {
        $cookie_header = '';
        foreach ($this->cookies as $k => $v) {
            $cookie_header .= $k . "=" . urlencode($v) . "; ";
        }
        $cookie_header .= substr($cookie_header, 0, -2);

        return $cookie_header;
    }

    public function post($url, $post_vars = array(), $post_files = array())
    {
        settype($post_vars, 'array');
        settype($post_files, 'array');
        $post_body = $this->getPostBody($post_vars, $post_files);

        return $this->request($url, 'POST', $this->_submit_type, $post_body);
    }

    public function get($url, $content_type = '')
    {
        return $this->request($url, 'GET', $content_type, '');
    }

    public function request($url = '', $http_method = '', $content_type = '', $body = '')
    {
        $headers = $http_method . " " . $url . " " . $this->_http_version . "\r\n";
        // HOST
        if (!empty($URI_PARTS['host'])) {
            $header_host = $URI_PARTS['host'];
            if (!empty($URI_PARTS['port'])) {
                $header_host .= ":" . $URI_PARTS['port'];
            }
            $this->setHeader('Host', $header_host);
        }
        // COOKIE
        if (!empty($this->cookies)) {
            $this->setHeader('Cookies', $this->getCookies());
        }
        // CONTENT TYPE
        if (!empty($content_type)) {
            $content_header = $content_type;
            if ($content_type == "multipart/form-data") {
                $content_header .= "; boundary=" . $this->_mime_boundary;
            }
            $this->setHeader('Content-type', $content_header);
        }
        if (!empty($body)) {
            $this->setHeader('Content-length', strlen($body));
        }

        if (!empty($URI_PARTS['user']) || !empty($URI_PARTS['pass'])) {
            $this->setHeader('Authorization', 'Basic ' . base64_encode($URI_PARTS['user'] . ':' . $URI_PARTS['user']));
        }
        // Пишем заголовки
        foreach ($this->headers as $name => $val) {
            $headers .= $name . ': ' . $val . "\r\n";
        }
        $headers .= "\r\n";

        return $headers . $body;


    }

    public function getPostBody($post_vars, $post_files)
    {
        $post_string = '';
        if (count($post_vars) == 0 && count($post_files) == 0) {
            return '';
        }
        if (!empty($post_vars)) {
            $post_string .= $this->getPostVars($post_vars);
        }
        if ($this->_submit_type == "multipart/form-data" && !empty($post_files)) {
            $post_string .= $this->getPostFiles($post_files);
        }
        if ($this->_submit_type == "multipart/form-data") {
            $post_string .= "--" . $this->_mime_boundary . "--\r\n";
        }

        return $post_string;
    }

    public function getPostVars($post_vars, $start_key_name = '')
    {
        $post_string = '';
        switch ($this->_submit_type) {
            case "application/x-www-form-urlencoded":
                foreach ($post_vars as $key => $val) {
                    if (!empty($start_key_name)) {
                        $key = '[' . urlencode($key) . ']';
                    }
                    if (is_array($val)) {
                        $post_string .= $this->getPostVars($val, $start_key_name . $key);
                    } else {
                        $post_string .= $start_key_name . $key . "=" . urlencode($val) . "&";
                    }
                }
                break;

            case "multipart/form-data":
                foreach ($post_vars as $key => $val) {
                    if (!empty($start_key_name)) {
                        $key = '[' . $key . ']';
                    }
                    if (is_array($val)) {
                        $post_string .= $this->getPostVars($val, $start_key_name . $key);
                    } else {
                        $post_string .= "--" . $this->_mime_boundary . "\r\n";
                        $post_string .= "Content-Disposition: form-data; name=\"" . $start_key_name . $key . "\"\r\n\r\n";
                        $post_string .= "$val\r\n";
                    }
                }
                break;
        }

        return $post_string;
    }

    public function getPostFiles($post_files, $start_key_name = '')
    {
        $post_string = '';
        foreach ($post_files as $key => $file_name) {
            if (!empty($start_key_name)) {
                $key = '[' . $key . ']';
            }
            if (is_array($file_name)) {
                $post_string .= $this->getPostFiles($file_name, $start_key_name . $key);
            } else {
                if (!is_readable($file_name)) {
                    continue;
                }
                $fp = fopen($file_name, "r");
                $file_content = fread($fp, filesize($file_name));
                fclose($fp);
                $base_name = basename($file_name);

                $post_string .= "--" . $this->_mime_boundary . "\r\n";
                $post_string .= "Content-Disposition: form-data; name=\"" . $start_key_name . $key . "\"; filename=\"$base_name\"\r\n\r\n";
                $post_string .= "$file_content\r\n";
            }
        }

        return $post_string;
    }

    public function parseUrl($url)
    {
        $URI_PARTS = parse_url($url);
        foreach (array('scheme', 'host', 'port', 'user', 'pass', 'path', 'query', 'fragment') as $var) {
            if (!empty($URI_PARTS[$var])) {
                $this->$$var = $URI_PARTS[$var];
            }
        }
    }
}

class bruterProcessAddAction extends waViewAction
{
    protected $process_data = array();

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $this->setProcessData();
        } else {
            $this->setLayout(new bruterBackendLayout());
            $passwordListModel = new bruterPasswordListModel();
            $lists = $passwordListModel->getLists();
            $this->view->assign(array('lists' => $lists));
        }
    }

    public function setProcessData()
    {
        $this->process_data['type'] = waRequest::post('type', 0, 'int');
        switch ($this->process_data['type']) {
            case 5:
            case 6:
                $this->process_data['protocol'] = waRequest::post('ftp_protocol');
                break;
            default:
                $this->process_data['protocol'] = waRequest::post('protocol');
                break;
        }
        $this->process_data['port'] = waRequest::post('port');
        $this->setDomains();
        $this->process_data['form_url'] = waRequest::post('form_url');
        $this->process_data['auth_type'] = waRequest::post('auth_type');//form_auth base_auth
        $this->process_data['login'] = waRequest::post('login');// field value values
        $this->process_data['password'] = waRequest::post('password');// field type list spisok file
        $this->process_data['cookies'] = waRequest::post('cookies');// cookies[0][name]
        $this->process_data['indication'] = waRequest::post('indication');//type value
        $this->process_data['sleep'] = waRequest::post('sleep');
        var_export($this->process_data);
        $sss = explode("\n", $this->process_data['password']['spisok']);
        foreach ($sss as $k => $v) {
            $sss[$k] = trim($v);
        }
        var_export($sss);
        var_export($_FILES);
        foreach ($_FILES as $k => $v) {
            $files[$k] = $v['name'];
        }
        var_export($files);
        $files = array(
            'dfsdfd' => $_SERVER['DOCUMENT_ROOT'] . '/test/22.php',
            'arr'    => array(
                'sdff' => $_SERVER['DOCUMENT_ROOT'] . '/test/55.php',
                0      => array(
                    'nuuu' => $_SERVER['DOCUMENT_ROOT'] . '/test/22.php',
                ),
            ),
        );
        $regf = new httpRequest('foxtop.ru', 80);
        $regf->setCookies(array('dfg' => '45645', 'dfsdf' => '234324'));
        $regf->setReferer('http://foxtop.ru/');
        $regf->setSubmitMultipart();
        var_dump($regf->post('/test/f.php', array('fghhd' => 'dfg', 'sdfsdf', array(0 => array('sdfdsf' => 'sdfsdf'))), $files));
    }

    public function setDomains()
    {
        $this->process_data['domains'] = array();
        if ($this->process_data['type'] == 3 || $this->process_data['type'] == 4) {
            $domains = waRequest::post('domains');
            $domains = explode("\n", $domains);
            foreach ($domains as $v) {
                $v = trim($v);
                if (!empty($v)) {
                    $this->process_data['domains'][] = $v;
                }
            }
        } else {
            $this->process_data['domains'] = waRequest::post('domain');
        }
    }
}