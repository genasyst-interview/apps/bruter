<?php

/**
 * Password recovery action /forgotpassword
 * Экшен восстановления пароля /forgotpassword
 * @see https://www.webasyst.com/framework/docs/dev/auth-frontend/
 */
class bruterForgotpasswordAction extends waForgotPasswordAction
{
    public function execute()
    {
        $this->setLayout(new bruterFrontendLayout());
        $this->setThemeTemplate('forgotpassword.html');
        parent::execute();
    }
}