<?php


class bruterPasswordListAddAction extends waViewAction
{

	public function execute()
	{
		$this->setLayout(new bruterBackendLayout());
		if(waRequest::method()=='post') {
			$name = waRequest::post('name');
			$passwords  = waRequest::post('passwords');
			$passwords_arr = explode("\n",$passwords);

			// pishem
			$passwordListModel = new bruterPasswordListModel();
			$passwordListModel->addList($name,$passwords_arr);
			$this->redirect('?module=password&action=list');
		}
	}
}