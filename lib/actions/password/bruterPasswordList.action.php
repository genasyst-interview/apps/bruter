<?php


class bruterPasswordListAction extends waViewAction
{

	public function execute()
	{
		$this->setLayout(new bruterBackendLayout());
		$passwordListModel = new bruterPasswordListModel();
		$lists = $passwordListModel->getLists();
		$this->view->assign(array('lists'=>$lists));

	}
}