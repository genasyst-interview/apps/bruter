<?php

/**
 * Login form action /login
 * Экшен формы логина /login
 * @see https://www.webasyst.com/framework/docs/dev/auth-frontend/
 */
class bruterLoginAction extends waLoginAction
{

    public function execute()
    {
        $this->setLayout(new bruterFrontendLayout());
        $this->setThemeTemplate('login.html');
        parent::execute();
    }

}